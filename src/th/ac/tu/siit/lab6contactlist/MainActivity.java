package th.ac.tu.siit.lab6contactlist;
import java.io.*;
import java.util.*;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.registerForContextMenu(this.getListView());
		list = new ArrayList<Map<String,String>>();
		File infile = getBaseContext().getFileStreamPath("contact.tsv");
		if (infile.exists()) { // if file is available
			try { // load data form file and display
				Scanner sc = new Scanner(infile);
				while(sc.hasNextLine()) {
					String line = sc.nextLine();
					String[] fields = line.split("\t");
					Map<String,String> m = new HashMap<String,String>();
					m.put("name", fields[0]);
					m.put("phone", fields[1]);
					m.put("type", fields[2]);
					if (fields.length > 3){
						m.put("email", fields[3]);
					}
					else {
						m.put("email","");
					}
					list.add(m);
				}
				sc.close();
			} catch (FileNotFoundException e) {
				//Do nothing
			}
		}
		
		adapter = new SimpleAdapter(this, list, R.layout.item,
				new String[] {"name", "phone", "type", "email"},
				new int[] {R.id.tvName, R.id.tvPhone, R.id.ivPhoneType,R.id.tvEmail}
		);
		this.setListAdapter(adapter);
	}

	@Override
	public void onBackPressed() { //if back button is press, the data wouldn't be save
		super.onBackPressed();
		this.onSaveInstanceState(null);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) { //save data from the activity to a file
		if (outState != null) {
			super.onSaveInstanceState(outState);
		}
		try {
			FileOutputStream outfile = openFileOutput("contact.tsv", MODE_PRIVATE); //mode private = only this app can use this file
			PrintWriter p = new PrintWriter(outfile);
			
			for(Map<String,String> m : list) {
				p.write(m.get("name")+"\t"+m.get("phone")+"\t"+m.get("type")+"\t"+m.get("email")+"\n"); // one line for one record
			}
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch(id) {
		case R.id.action_new:
			Intent i1 = new Intent(this, AddNewActivity.class);
			startActivityForResult(i1, 9999);
			return true;
		case R.id.action_about:
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("About");
			dialog.setMessage("This application is developed by " +
					"Cholwich Nattee for SIIT ITS333.\n" +
					"All the icons used in the application are from " +
					"http://www.wpzoom.com/wpzoom/new-freebie-wpzoom-" +
					"developer-icon-set-154-free-icons/");
			dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", 
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}});
			dialog.show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			Map<String,String> m = new HashMap<String,String>();
			m.put("name", data.getStringExtra("name"));
			m.put("phone", data.getStringExtra("phone"));
			String sType = data.getStringExtra("type");
			m.put("email", data.getStringExtra("email"));
			if (sType.compareTo("home") == 0) {
				m.put("type", Integer.toString(R.drawable.home));
			}
			else if (sType.compareTo("mobile") == 0) {
				m.put("type", Integer.toString(R.drawable.mobile));
			}
			else {
				m.put("type", Integer.toString(R.drawable.ic_launcher));
			}
			list.add(m);
			adapter.notifyDataSetChanged();
		}
		else if (requestCode == 8888 && resultCode == RESULT_OK) {
			Map<String,String> m = new HashMap<String,String>();
			m.put("name", data.getStringExtra("name"));
			m.put("phone", data.getStringExtra("phone"));
			String sType = data.getStringExtra("type");
			m.put("email", data.getStringExtra("email"));
			int position = data.getIntExtra("position", -1);
			if (sType.compareTo("home") == 0) {
				m.put("type", Integer.toString(R.drawable.home));
			}
			else if (sType.compareTo("mobile") == 0) {
				m.put("type", Integer.toString(R.drawable.mobile));
			}
			else {
				m.put("type", Integer.toString(R.drawable.ic_launcher));
			}
			if(position >=0)
			{
			list.set(position, m);
		
			adapter.notifyDataSetChanged();
			}
		}
		else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int id = item.getItemId();
		AdapterContextMenuInfo a =(AdapterContextMenuInfo)item.getMenuInfo();//Get the position of the item clicked
		int position = a.position;
	
		switch(id) {
		case R.id.action_edit:
			Intent i = new Intent(this,AddNewActivity.class); //Create an intent for starting add new activity
			Map<String,String> m = list.get(position); //Attach the selected item as extra for the intent
			i.putExtra("name", m.get("name"));
			i.putExtra("phone", m.get("phone"));
			i.putExtra("type", m.get("type"));
			i.putExtra("email", m.get("email"));
			i.putExtra("position", position);
			startActivityForResult(i,8888); //Start AddNewActivity for result(using different request code)
			Toast e = Toast.makeText(this, "Edit menu is selected", 
					Toast.LENGTH_SHORT);
			e.show();
			return true;
		case R.id.action_del:
			list.remove(position);
			adapter.notifyDataSetChanged();
			Toast d = Toast.makeText(this, "Delete menu is selected", 
					Toast.LENGTH_SHORT);
			d.show();
			return true;
		}
		return super.onContextItemSelected(item);
	}
}
